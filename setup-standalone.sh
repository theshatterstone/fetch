#!/usr/bin/env bash

echo "Setting up (pc)fetch..."

#cp -rf ./fetch.sh ~/.local/bin/pcfetch

mkdir -p ~/.local/bin

curl https://gitlab.com/theshatterstone/fetch/-/raw/main/fetch.sh -o ~/.local/bin/pcfetch

chmod +x ~/.local/bin/pcfetch

echo "Done!"

echo "Make sure to add ~/.local/bin to \$PATH if you haven't already done so!"
