#!/usr/bin/env python3

import os
import string
import platform

os_name = platform.system()
#os_name = "Darwin"
os_ver = platform.release()

if os_name == "Linux":
    #What the uncommented line below does: checks os release file, focuses on the lines with "NAME", picks the first one, cuts off the "NAME=" part, and removes all numbers, then special characters from the name.
    #opsys = os.system('cat /etc/os-release | grep "NAME" | head -n +1 | cut -b 6- | tail -c +2 | head -c -2 | sed "s/[0-9]\+$//" | tr -cd "[:alnum:]\"\n')
    command = 'cat /etc/os-release | grep "NAME" | head -n +1 | cut -b 6- | sed "s/[0-9]\+$//" '
    cmd = "/bin/bash -c " + command
    #opsys.rstrip(string.digits)
elif os_name == "Darwin":
    opsys = "MacOS"
elif os_name == "Windows":
    opsys = "Windows"
    opsys = str(opsys) + " " + str(os_ver)
else:
    opsys = "Unknown OS / BSD"

#print(ord("0"))
#print (opsys)
#print (" ")
opsys = os.system(cmd)
