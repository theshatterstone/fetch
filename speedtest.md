## The results from my speedtest are as follows:

### Neofetch: 
```neofetch  0.04s user 0.05s system 103% cpu 0.089 total```

### Neofetch (color_blocks off, as I normally run it):
```neofetch --color_blocks off  0.04s user 0.05s system 104% cpu 0.088 total```

### PCFetch:
```pcfetch  0.02s user 0.03s system 111% cpu 0.041 total```
