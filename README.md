# My fetch (pcfetch)

This is a simple fetch script I made to try out bash scripting by making something useful. 

The ASCII art used in this script is made by CA15 and can be found at https://www.asciiart.eu/computers/computers 

### Setup

```
git clone https://gitlab.com/theshatterstone/fetch
mkdir -p $HOME/.local/bin
cd fetch/
./setup.sh
```

### Standalone (Easier, no-repo, curl) approach

Remember to read the script first! And the fetch source! It's called security.
```
curl https://gitlab.com/theshatterstone/fetch/-/raw/main/setup-standalone.sh | bash
```

### Performance

To see evidence of the performance improvement compared to the same
setup in neofetch, see [Speed Test Results](./speedtest.md "Link to Speed Test Results")

### Credits

I used a modified version of the ASCII art, so as a way of crediting the original creator of it, 
I'm including the original ASCII art below:

     ___________
    ||         ||            _______
    ||ASCII ART||           | _____ |
    ||         ||           ||_____||
    ||_________||           |  ___  |
    |  + + + +  |           | |___| |
        _|_|_   \           |       |
       (_____)   \          |       |
                  \    ___  |       |
           ______  \__/   \_|       |
          |   _  |      _/  |       |
          |  ( ) |     /    |_______|
          |___|__|    /         CA15
               \_____/
