#!/usr/bin/env bash
# This is a sysfetch script to show system information, as well as a cool logo to go with it,
# inspired by Neofetch. This is my own custom script, with any due credits being given in the README:
#
#
os=$(hostnamectl | grep "Operating System" | cut -d ':' -f2)
de=$( { echo $XDG_CURRENT_DESKTOP; echo $DESKTOP_SESSION | awk -F / '{print $NF}'; } | grep -v '^$' | head -n 1 | awk -F/ '{print $NF}' )
kernel=$(uname -r)
uptime="$(uptime | awk -F, '{sub(".*up ",x,$1);print $1}' | sed -e 's/^[ \t]*//')"
battery=$(cat /sys/class/power_supply/BAT1/capacity)
batstat=$(cat /sys/class/power_supply/BAT1/status)
cpu=$(cat /proc/cpuinfo | grep "name" | uniq | cut -b 14-)
shell=$(echo $SHELL | awk -F / '{print $NF}' )
if [ -x "$(command -v tput)" ]; then
	bold="$(tput bold 2> /dev/null)"
	black="$(tput setaf 0 2> /dev/null)"
	red="$(tput setaf 1 2> /dev/null)"
	green="$(tput setaf 2 2> /dev/null)"
	yellow="$(tput setaf 3 2> /dev/null)"
	blue="$(tput setaf 4 2> /dev/null)"
	magenta="$(tput setaf 5 2> /dev/null)"
	cyan="$(tput setaf 6 2> /dev/null)"
	white="$(tput setaf 7 2> /dev/null)"
	reset="$(tput sgr0 2> /dev/null)"
fi

# you can change these
lc="${reset}${bold}${blue}"         # labels
nc="${reset}${bold}${blue}"         # user and hostname
ic="${reset}"                       # info
c0="${reset}${bold}${blue}"         # first color

## OUTPUT

cat <<EOF

${c0}  _____________  | ${nc}${USER}${ic}@${nc}${HOSTNAME}${reset}
${c0}  ||         ||  | ${lc}OS: ${ic}${os}${reset}
${c0}  ||         ||  | ${lc}KERNEL: ${ic}${kernel}${reset}
${c0}  ||         ||  | ${lc}UPTIME: ${ic}${uptime}${reset}
${c0}  ||_________||  | ${lc}SHELL: ${ic}${shell}${reset}
${c0}  ||  + + +  ||  | ${lc}TERMINAL: ${ic}${TERM}${reset}
${c0}      _|_|_      | ${lc}DE: ${ic}${de}${reset}
${c0}     (_____)     | ${lc}BATTERY: ${ic}${battery}%, ${batstat}${reset} 
EOF

